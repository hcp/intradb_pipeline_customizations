#!/bin/bash

#################################################################################################
## NOTE:  This version names files according to the latest file naming convention (2011-12-15) ##
#################################################################################################

if [ $# -lt 8 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export WORK_DIR=$4
export PROJECT=$5
export SUBJECT=$6
export SESSION=$7
export EXISTING=$8

if [ ! -d "$WORK_DIR" ] ; then
	echo "ERROR:  Specified directory does not exist"
	exit 9
fi

pushd $WORK_DIR

HOST=`echo $HOST | sed 's/\/$//'`

## BACK UP ORIGINAL VERSIONS OF SCANS, STUDIES FILES
cp -p $WORK_DIR/scans.txt $WORK_DIR/scans_orig.txt
cp -p $WORK_DIR/studies.txt $WORK_DIR/studies_orig.txt

## MAIN PROCESSING METHOD (PROCESSES BY SCAN)
function process {

	SCAN=$1
	echo "RUNNING PROCESS - SCAN=$1"

	## MODIFICATIONS ACCORDING TO NAMING CONVENTION ##
	echo curl -k -u $USER:$PASSWORD -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?format=xml
	export SCANTYPE=`curl -k -u $USER:$PASSWORD -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?format=xml | grep -a " ID=" | grep -a " type=\"" | sed "s/^.* type=\"//" | sed "s/\".*$//"`
	export SERIESDESC=`curl -k -u $USER:$PASSWORD -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?format=xml | grep -a "series_description" | sed "s/^[^>]*>//" | sed "s/<.*$//"`
	## MRH (2012/04/26) - remove spaces in scantype/seriesdesc - bugfix for transfer failures
	SCANTYPE=`echo $SCANTYPE | sed "s/ //g"`
	SERIESDESC=`echo $SERIESDESC | sed "s/ //g"`
	echo SERIESDESC=$SERIESDESC
	echo SCANTYPE=$SCANTYPE
	
	if [[ $SERIESDESC =~ ^BIAS ]] || [[ $SERIESDESC =~ ^AFI ]] || [[ $SERIESDESC =~ ^T[12]W*.* ]] ;  then
	
		echo "Scan SeriesDescription (SCAN=$SCAN, SERIESDESC=$SERIESDESC) indicates eligibility for facemasking.  Continue processing..."
	
	else 
	
		echo "SeriesDescription ($SERIESDESC) will be skipped..."
		echo "Scan excluded based on SeriesDescription (SCAN=$SCAN, SERIESDESC=$SERIESDESC)" 
		return
	
	fi

	## SEE IF ALREADY PROCESSED.  ACT ACCORDING TO "EXISTING" PARAMETER (SKIP/OVERWRITE/FAIL) ##
	
	export DEFACE_RESP=`curl -k -s -L -o /dev/null -w '%{http_code}' -u $USER:$PASSWORD -X GET "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM_DEFACED?format=xml"`
	export DEFACEQC_RESP=`curl -k -s -L -o /dev/null -w '%{http_code}' -u $USER:$PASSWORD -X GET "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DEFACE_QC?format=xml"`
	
	if [[ ! $EXISTING =~ ^[FOS] ]] ; then
	
		EXISTING=FAIL
	
	fi
	
	if [[ $EXISTING =~ ^F  && ( $DEFACE_RESP -eq 200 || $DEFACEQC_RESP -eq 200 ) ]]; then
	
		echo "ERROR:  Defaced output already exists [SCAN=$SCAN].  Pipeline set to fail on existing.  Exiting...."
		shopt -u nocasematch
		exit 9
	
	fi
	
	if [[ $EXISTING =~ ^O ]]; then 
	
		if [ $DEFACE_RESP -eq 200 ]; then
	
			echo "NOTE:  Defaced output already exists [SCAN=$SCAN] and [EXISTING=OVERWRITE].  Removing current output"
			if [[ ${#PROJECT} -gt 0 &&  ${#SUBJECT} -gt 0 &&  ${#SESSION} -gt 0 &&  ${#SCAN} -gt 0 ]] ; then
				curl -k -u $USER:$PASSWORD -X DELETE "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM_DEFACED"
			fi
	
		fi
		if [ $DEFACEQC_RESP -eq 200 ]; then
	
			echo "NOTE:  Defaced QC output already exists [SCAN=$SCAN] and [EXISTING=OVERWRITE].  Removing current output"
			if [[ ${#PROJECT} -gt 0 &&  ${#SUBJECT} -gt 0 &&  ${#SESSION} -gt 0 &&  ${#SCAN} -gt 0 ]] ; then
				echo "HELLO"
				curl -k -u $USER:$PASSWORD -X DELETE "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DEFACE_QC"
			fi
	
		fi
	
	fi
	
	if [[ $EXISTING =~ ^S && ( $DEFACE_RESP -eq 200 || $DEFACEQC_RESP -eq 200 ) ]]; then
	
		echo "NOTE:  Defaced output already exists [SCAN=$SCAN] and [EXISTING=SKIP].  Skipping scan..."
		echo "Scan excluded because defacing output already exists and parameter EXISTING=SKIP" 
		return
	
	fi

	RUNVAL="${RUNVAL}${SCAN},"

}

shopt -s nocasematch

## LOOP OVER SCANS.TXT, RUNNING PROCESSING AND OUTPUTING MODIFIED FILES
RUNVAL=""
IFS=',' read -ra ARR <<< "`cat $WORK_DIR/scans.txt`"
for SCAN in "${ARR[@]}"; do
	echo $SCAN
	process $SCAN
done

shopt -u nocasematch

if [ "${#RUNVAL}" -lt 1 ]; then
   echo "ERROR:  No scans are selected for processing - exiting"
   exit 9
fi

echo $RUNVAL > $WORK_DIR/scans.txt
echo $RUNVAL | sed "s/[0-9]\+,/study&/g" > $WORK_DIR/studies.txt

popd


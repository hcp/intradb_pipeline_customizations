#!/bin/bash

if [ $# -lt 8 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export OUT_DIR=$4
export PROJECT=$5
export SUBJECT=$6
export SESSION=$7
export SCAN=$8

if [ ! -d "$OUT_DIR" ] ; then
   echo "ERROR:  Specified directory does not exist"
   exit 1
fi

cd $OUT_DIR

#######################################################################################################################
## WHEN MULTIPLE NIFTI FILES, THERE IS USUALLY A BASE-NAMED FILE WITH A,B,C... APPENDED AFTER SUBSEQUENT FILE NAMES. ##
## WE'LL RENAME THE FILES REPLACING BASE NAME WITH SCAN NAME FOR ALL FILES.  THEN WE'LL UPLOAD ALL DIRECTORY OUTPUT. ##
#######################################################################################################################

BASENAME=`find . -name "*.nii*" | sed -e "s/^.*\///" | sed -e "s/[.].*$//" | sort | head -1`

echo "BASENAME=$BASENAME"

if [ -z "$BASENAME" ] ; then
   echo "ERROR:  No NIFTI files exist under specified directory"
   exit 1
fi

##############################################################
## LOOP OVER FILES GENERATING ARCHIVE NAME AND SENDING THEM ##
##############################################################

for FIL in $(find . -type f)
do

   echo "ORIFN=$FIL"

   NEWFN=`echo "$FIL" | sed s/$BASENAME/$SCAN/`

   echo "NEWFN=$NEWFN"

   #### COMMENT THIS SECTION OUT.  DON'T NEED TO RENAME THE FILES
   ##if [ "$NEWFN" != "$FIL" ] ; then
   ##   mv $FIL $NEWFN
   ##   FIL=$NEWFN
   ##fi

   SENDFN=`echo "$NEWFN" | sed -e "s/^.*\///"`
   LOCALPATH=`echo "$FIL" | sed "#^.#$(pwd)#"`
   REMOTEPATH="/data/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/NIFTI/files/${SENDFN}"

   echo "SENDFN=$SENDFN"

   #################
   ## UPLOAD FILE ##
   #################

   #echo "/data/intradb/pipeline/other-tools/xnat_tools/XNATRestClient -u $USER -p $PASSWORD -host $HOST -remote $REMOTEPATH -local $LOCALPATH -m PUT"
   #/data/intradb/pipeline/other-tools/xnat_tools/XNATRestClient -u $USER -p $PASSWORD -host $HOST -remote $REMOTEPATH -local $LOCALPATH -m PUT
   #echo curl -k -u ${USER}:${PASSWORD} -X PUT -T "$LOCALPATH" ${HOST}${REMOTEPATH}?extract=false\&inbody=true\&format=NIFTI\&content=RAW
   curl -k -u ${USER}:${PASSWORD} -X PUT -T "$LOCALPATH" ${HOST}${REMOTEPATH}?extract=false\&inbody=true\&format=NIFTI\&content=RAW

done


#!/bin/bash

#################################################################################################
## NOTE:  This version names files according to the latest file naming convention (2011-12-15) ##
#################################################################################################

## BREAKING RENAMING OUT INTO SEPARATE STEP AND PERFORMING EARLIER SO UNZIPPED NIFTI FILES HAVE ##
## THE SAME NAME AS THE ZIPPED VERSION.                                                         ##

if [ $# -lt 8 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export OUT_DIR=$4
export PROJECT=$5
export SUBJECT=$6
export SESSION=$7
export SCAN=$8

## SET UP FSL
export FSLDIR=/nrgpackages/tools.release/fsl-4.1.9-nrg
source /nrgpackages/tools.release/fsl-4.1.9-nrg/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

if [ ! -d "$OUT_DIR" ] ; then
   echo "ERROR:  Specified directory does not exist"
   exit 1
fi

cd $OUT_DIR

## CHANGE FOR DEFACED PIPELINE ##
if [ `find . -type f | wc -l` -le 0 ] ; then

   ## Okay here.  Only T1, T2 should have files if this is a renaming run.
   exit 0

fi 
## END CHANGE FOR DEFACED PIPELINE ##

#######################################################################################################################
## WHEN MULTIPLE NIFTI FILES, THERE IS USUALLY A BASE-NAMED FILE WITH A,B,C... APPENDED AFTER SUBSEQUENT FILE NAMES. ##
## WE'LL RENAME THE FILES REPLACING BASE NAME WITH SCAN NAME FOR ALL FILES.  THEN WE'LL UPLOAD ALL DIRECTORY OUTPUT. ##
#######################################################################################################################

BASENAME=`find . -name "*.nii*" | sed -e "s/^.*\///" | sed -e "s/[.].*$//" | sort | head -1`

echo "BASENAME=$BASENAME"

if [ -z "$BASENAME" ] ; then
   echo "ERROR:  No NIFTI files exist under specified directory"
   exit 1
fi

## MODIFICATIONS ACCORDING TO NAMING CONVENTION ##
export SCANTYPE=`curl -k -u ${USER}:${PASSWORD} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?format=xml | grep -a " ID=" | grep -a " type=\"" | sed "s/^.* type=\"//" | sed "s/\".*$//"`
export SERIESDESC=`curl -k -u ${USER}:${PASSWORD} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?format=xml | grep -a "series_description" | sed "s/^[^>]*>//" | sed "s/<.*$//"`
## MRH (2012/04/26) - remove spaces in scantype/seriesdesc - bugfix for transfer failures 
SCANTYPE=`echo ${SCANTYPE} | sed "s/ //g"`
SERIESDESC=`echo ${SERIESDESC} | sed "s/ //g"`

echo "SCANTYPE=$SCANTYPE"
echo "SERIESDESC=$SERIESDESC"


## MODIFY SERIES DESCRIPTION AND SCAN TYPE FOR FIELD MAP SESSIONS 
if [[ ${SERIESDESC} =~ [Ff][Ii][Ee][Ll][Dd][Mm][Aa][Pp][0-9]* ]] && [[ ${SERIESDESC} != *_* ]] ;  then

   SD_APPEND=""
   for NFIL in $(find . -name "*.nii*")
   do

      echo "fslval output ($NFIL)=`${FSLDIR}/bin/fslval $NFIL dim4`"
      ## ADDED PER MATT G (2012/07/13) 
      if [[ "`${FSLDIR}/bin/fslval $NFIL dim4`" -eq "2"  ]] ;  then
         SD_APPEND="_Magnitude"
      elif [[ "`${FSLDIR}/bin/fslval $NFIL dim4`" -eq "1"  ]] ;  then
         SD_APPEND="_Phase"
      fi

   done

   if [ -n "${SD_APPEND}" ] ; then
      SERIESDESC="${SERIESDESC}${SD_APPEND}"
      curl -k -u ${USER}:${PASSWORD} -X PUT "${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?series_description=$SERIESDESC&type=FieldMap"
   fi

fi

## END MODIFICATIONS ACCORDING TO NAMING CONVENTION ##

NEWPART=${SESSION}_${SERIESDESC}

####################################
## LOOP OVER FILES RENAMEING THEM ##
####################################

for FIL in $(find . -type f)
do

   echo "ORIFN=$FIL"

   echo "BASENAME=$BASENAME"
   echo "NEWPART=$NEWPART"
   NEWFN=`echo "$FIL" | sed s/$BASENAME/$NEWPART/`

   echo "NEWFN=$NEWFN"

   if [ "$NEWFN" != "$FIL" ] ; then
      mv $FIL $NEWFN
   fi

done


/* Modified version of /userland/cvs/pixelmed/imgbook/com/pixelmed/dicom/DescriptionFactory.java,v 1.12 2011/12/29 15:26:15 dclunie 
 * Reduced and modified by MRH for computing and printing plane angle */
import java.lang.*;
import java.util.*;

/**
 * <p>A class of static methods to provide descriptions of images, including image orientation
 * relative to the patient from the mathematical position and orientation attributes,
 * and including other descriptive attributes such as from dicom directory records and
 * images using multi-frame functional groups.</p>
 *
 * @author	dclunie
 */
//abstract public class DescriptionFactory {
public class ComputePlaneAngleFromImageOrientationPatient {

	/***/
	// 0.5477 would be the square root of 1 (unit vector sum of squares) divided by 3 (oblique axes - a "double" oblique)
	// 0.7071 would be the square root of 1 (unit vector sum of squares) divided by 2 (oblique axes)
	/***/
	private static final double obliquityThresholdCosineValue = 0.8;
	private static final double PI = 3.1416;

	public static void main(String[] args) {
		String ins = args[0];
		//System.out.println(ins);
		String[] oArray = ins.split("\\\\");
		double[] nums = new double[oArray.length];
		for (int i = 0; i < oArray.length; i++) {
			//System.out.println( "oArray[i]=" + oArray[i] );
    			nums[i] = Double.parseDouble(oArray[i]);
		}
		System.out.println(computePlaneAngleFromImageOrientationPatient(nums[0],nums[1],nums[2],nums[3],nums[4],nums[5]));
		//System.out.println(makeImageOrientationLabelFromImageOrientationPatient(nums[0],nums[1],nums[2],nums[3],nums[4],nums[5]));
		//System.out.println(makePatientOrientationFromImageOrientationPatient(nums[0],nums[1],nums[2],nums[3],nums[4],nums[5]) + "\n\n");
	}

	/**
	 * <p>Get a label describing the major axis from a unit vector (direction cosine) as found in ImageOrientationPatient.</p>
	 *
	 * <p>Some degree of deviation from one of the standard orthogonal axes is allowed before deciding no major axis applies and returning null.</p>
	 *
	 * @param	x
	 * @param	y
	 * @param	z
	 * @return		the string describing the orientation of the vector, or null if oblique
	 */
	public static final String getMajorAxisFromPatientRelativeDirectionCosine(double x,double y,double z) {
		String axis = null;
		
		String orientationX = x < 0 ? "R" : "L";
		String orientationY = y < 0 ? "A" : "P";
		String orientationZ = z < 0 ? "F" : "H";

		double absX = Math.abs(x);
		double absY = Math.abs(y);
		double absZ = Math.abs(z);

		// The tests here really don't need to check the other dimensions,
		// just the threshold, since the sum of the squares should be == 1.0
		// but just in case ...
		
		if (absX>obliquityThresholdCosineValue && absX>absY && absX>absZ) {
			axis=orientationX;
		}
		else if (absY>obliquityThresholdCosineValue && absY>absX && absY>absZ) {
			axis=orientationY;
		}
		else if (absZ>obliquityThresholdCosineValue && absZ>absX && absZ>absY) {
			axis=orientationZ;
		}
		return axis;
	}
	
	public static final String AXIAL_LABEL = "AXIAL";
	public static final String CORONAL_LABEL = "CORONAL";
	public static final String SAGITTAL_LABEL = "SAGITTAL";
	public static final String OBLIQUE_LABEL = "OBLIQUE";

	/**
	 * <p>Get a label describing the axial, coronal or sagittal plane from row and column unit vectors (direction cosines) as found in ImageOrientationPatient.</p>
	 *
	 * <p>Some degree of deviation from one of the standard orthogonal planes is allowed before deciding the plane is OBLIQUE.</p>
	 *
	 * @param	rowX
	 * @param	rowY
	 * @param	rowZ
	 * @param	colX
	 * @param	colY
	 * @param	colZ
	 * @return		the string describing the plane of orientation, AXIAL, CORONAL, SAGITTAL or OBLIQUE, or null if undetermined
	 */
	public static double computePlaneAngleFromImageOrientationPatient(
			double rowX,double rowY,double rowZ,
			double colX,double colY,double colZ) {
		String label = makeImageOrientationLabelFromImageOrientationPatient(rowX,rowY,rowZ,colX,colY,colZ);
		double plane_angle = 999999;
		if (label.equals(AXIAL_LABEL)) {
			plane_angle = 180*Math.acos(colY)/PI;
			if (colZ>0) plane_angle = - plane_angle;
		} else if (label.equals(CORONAL_LABEL)) {
			plane_angle = 180*Math.acos(colZ)/PI;
		} else if (label.equals(SAGITTAL_LABEL)) {
			plane_angle = (180 - 180*Math.acos(colZ)/PI);
		}
		return plane_angle;
	}


	/**
	 * <p>Get a label describing the axial, coronal or sagittal plane from row and column unit vectors (direction cosines) as found in ImageOrientationPatient.</p>
	 *
	 * <p>Some degree of deviation from one of the standard orthogonal planes is allowed before deciding the plane is OBLIQUE.</p>
	 *
	 * @param	rowX
	 * @param	rowY
	 * @param	rowZ
	 * @param	colX
	 * @param	colY
	 * @param	colZ
	 * @return		the string describing the plane of orientation, AXIAL, CORONAL, SAGITTAL or OBLIQUE, or null if undetermined
	 */
	public static final String makeImageOrientationLabelFromImageOrientationPatient(
			double rowX,double rowY,double rowZ,
			double colX,double colY,double colZ) {
		String label = null;
		String rowAxis = getMajorAxisFromPatientRelativeDirectionCosine(rowX,rowY,rowZ);
		String colAxis = getMajorAxisFromPatientRelativeDirectionCosine(colX,colY,colZ);
		if (rowAxis != null && colAxis != null) {
			if      ((rowAxis.equals("R") || rowAxis.equals("L")) && (colAxis.equals("A") || colAxis.equals("P"))) label=AXIAL_LABEL;
			else if ((colAxis.equals("R") || colAxis.equals("L")) && (rowAxis.equals("A") || rowAxis.equals("P"))) label=AXIAL_LABEL;
		
			else if ((rowAxis.equals("R") || rowAxis.equals("L")) && (colAxis.equals("H") || colAxis.equals("F"))) label=CORONAL_LABEL;
			else if ((colAxis.equals("R") || colAxis.equals("L")) && (rowAxis.equals("H") || rowAxis.equals("F"))) label=CORONAL_LABEL;
		
			else if ((rowAxis.equals("A") || rowAxis.equals("P")) && (colAxis.equals("H") || colAxis.equals("F"))) label=SAGITTAL_LABEL;
			else if ((colAxis.equals("A") || colAxis.equals("P")) && (rowAxis.equals("H") || rowAxis.equals("F"))) label=SAGITTAL_LABEL;
		}
		else {
			label=OBLIQUE_LABEL;
		}
		return label;
	}

	/**
	 * <p>Get a PatientOrientation style string from a unit vector (direction cosine) as found in ImageOrientationPatient.</p>
	 *
	 * <p>Returns letters representing R (right) or L (left), A (anterior) or P (posterior), F (feet) or H (head).</p>
	 *
	 * <p>If the orientation is not precisely orthogonal to one of the major axes,
	 * more than one letter is returned, from major to minor axes, with up to three
	 * letters in the case of a "double oblique".</p>
	 *
	 * @param	x
	 * @param	y
	 * @param	z
	 * @return		the string describing the orientation of the vector 
	 */
	public static final String makePatientOrientationFromPatientRelativeDirectionCosine(double x,double y,double z) {
		StringBuffer buffer = new StringBuffer();
		
		String orientationX = x < 0 ? "R" : "L";
		String orientationY = y < 0 ? "A" : "P";
		String orientationZ = z < 0 ? "F" : "H";

		double absX = Math.abs(x);
		double absY = Math.abs(y);
		double absZ = Math.abs(z);

		for (int i=0; i<3; ++i) {
			if (absX>.0001 && absX>absY && absX>absZ) {
				buffer.append(orientationX);
				absX=0;
			}
			else if (absY>.0001 && absY>absX && absY>absZ) {
				buffer.append(orientationY);
				absY=0;
			}
			else if (absZ>.0001 && absZ>absX && absZ>absY) {
				buffer.append(orientationZ);
				absZ=0;
			}
			else break;
		}
		return buffer.toString();
	}

	/**
	 * <p>Get a PatientOrientation style string from row and column unit vectors (direction cosines) as found in ImageOrientationPatient.</p>
	 *
	 * <p>Returns letters representing R (right) or L (left), A (anterior) or P (posterior), F (feet) or H (head).</p>
	 *
	 * <p>If the orientation is not precisely orthogonal to one of the major axes,
	 * more than one letter is returned, from major to minor axes, with up to three
	 * letters in the case of a "double oblique".</p>
	 *
	 * <p>The row and column letters returned are separated by the usual DICOM string delimiter, a backslash.</p>
	 *
	 * @param	rowX
	 * @param	rowY
	 * @param	rowZ
	 * @param	colX
	 * @param	colY
	 * @param	colZ
	 * @return		the string describing the row and then the column 
	 */
	public static final String makePatientOrientationFromImageOrientationPatient(
			double rowX,double rowY,double rowZ,
			double colX,double colY,double colZ) {
		return	 makePatientOrientationFromPatientRelativeDirectionCosine(rowX,rowY,rowZ)
			+"\\"
			+makePatientOrientationFromPatientRelativeDirectionCosine(colX,colY,colZ);
	}
	
}

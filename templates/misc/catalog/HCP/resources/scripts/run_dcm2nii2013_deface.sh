#!/bin/bash

if [ "${1}" == "-version" ] ||  [ "${1}" == "--version" ] ||  [ "${2}" == "-version" ] ||  [ "${2}" == "--version" ] ; then
   echo "1.0"
   exit 0
fi

## IMPORTANT:  When pointing to a new version, clear the dcm2nii.ini file in the ~/.dcm2nii directory and run the command
##             to generate a new one.  Then compare that file to the one we're using to look for changes that should be
##             made corresponding to the new version. 
COMMAND_LINE=/nrgpackages/tools.release/mricronlx64-2013.06.12/dcm2nii
OUT_DIR=`pwd`

while [ $# -ne 0 ] ; do
        if [ "${1}" == "-o" ]; then
                COMMAND_LINE="$COMMAND_LINE -o ."
                shift
                OUT_DIR="${1}"
        else
                COMMAND_LINE="$COMMAND_LINE $1"
        fi
        shift
done

COMMAND_LINE_DEFACE=`echo "${COMMAND_LINE}" | sed -e "s/\/RAW\//\/RAW_DEFACE\//" | sed -e "s/\/DICOM/\/DICOM_DEFACED/"`
OUT_DIR_RAW=`echo "$OUT_DIR" | sed -e "s/\/NIFTI\//\/NIFTI_RAW\//"` 
DEFACED_DICOM_DIR=`echo "$COMMAND_LINE_DEFACE" | sed -e "s/^.* //"`

echo "COMMAND_LINE=$COMMAND_LINE"
echo "COMMAND_LINE_DEFACE=$COMMAND_LINE_DEFACE"
echo "OUT_DIR=$OUT_DIR"
echo "OUT_DIR_RAW=$OUT_DIR_RAW"
echo "DEFACED_DICOM_DIR=$DEFACED_DICOM_DIR"

export STRUCT=false

if [ `find $DEFACED_DICOM_DIR -type f | wc -l` -gt 0 ] ; then

   STRUCT=true

fi

echo "STRUCT=$STRUCT"

if ! $STRUCT ; then

	pushd $OUT_DIR
	
	echo "Executing $COMMAND_LINE"
	$COMMAND_LINE
	
	rc=$?
	
	if [ $rc -ne 0 ] ; then
	
		echo "Executing(2) $COMMAND_LINE"
	        $COMMAND_LINE
	        rc=$?
	
	fi
	
	popd

else

	## CONVERT NON_DEFACED DATA
	
	pushd $OUT_DIR_RAW
	
	echo "Executing $COMMAND_LINE"
	$COMMAND_LINE
	
	rc=$?
	
	if [ $rc -ne 0 ] ; then
	
		echo "Executing(2) $COMMAND_LINE"
	        $COMMAND_LINE
	        rc=$?
	
	fi
	
	popd

	## CONVERT DEFACED DATA

	if [ -d $DEFACED_DICOM_DIR ] ; then

		pushd $OUT_DIR
		
		echo "Executing $COMMAND_LINE_DEFACE"
		$COMMAND_LINE_DEFACE
		
		rc=$?
		
		if [ $rc -ne 0 ] ; then
		
			echo "Executing(2) $COMMAND_LINE_DEFACE"
		        $COMMAND_LINE_DEFACE
		        rc=$?
		
		fi
		
		popd

	fi

fi

exit $rc


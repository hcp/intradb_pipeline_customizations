#!/bin/bash

#################################################################################################
## NOTE:  This version names files according to the latest file naming convention (2011-12-15) ##
#################################################################################################

## BREAKING RENAMING OUT INTO SEPARATE STEP AND PERFORMING EARLIER SO UNZIPPED NIFTI FILES HAVE ##
## THE SAME NAME AS THE ZIPPED VERSION.                                                         ##



if [ $# -lt 8 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export PROJECT=$4
export SUBJECT=$5
export SESSION=$6
export SCAN=$7
export INPUT_DIR=$8


export JSESSIONID=`curl -s -u $USER:$PASSWORD ${HOST}/data/JSESSION`

SCANVAR=`curl -k -s --cookie JSESSIONID=${JSESSIONID} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}?format=xml`
TYPEVAR=`echo -e "$SCANVAR" | grep -a "xnat:imageType>" | sed -e "s/<[^<]*>//g"`

echo "TYPEVAR=$TYPEVAR"

if [[ ! "$TYPEVAR" == *MOSAIC* ]] ; then

	echo "NOTE:  Not a mosaic scan, skip plane angle check"
	exit 0

else

	echo "NOTE:  Mosaic scan.  Begin plane angle check"

fi

if [ ! -d $INPUT_DIR ] ; then
	echo "ERROR:  Input directory does not exist ($INPUT_DIR)"
	exit 0
fi 

SCRIPTDIR=`dirname $0`
SCRIPTDIR=`readlink -f $SCRIPTDIR`

PLANE_ANGLE=$(dcmdump +L -q --scan-directories --search "0020,0037" $INPUT_DIR | grep "ImageOrientationPatient" | sort -u | sed -e "s/^.*\\[/\"/" -e "s/\].*$/\"/" -e "s/\\\\/&&/g" | xargs -I '{}' sh -c "java -cp '/nrgpackages/tools.release/intradb/pipeline_resources/java' ComputePlaneAngleFromImageOrientationPatient {}")
if [ "$?" -ne "0" ] ; then
	echo "ERROR:  System error computing plane angles"
	exit 1
fi

if [ `echo "$PLANE_ANGLE<0" | bc -l` -eq 1 ] ; then
	echo "ERROR:  Computed plane angle is less than zero [SCAN=$SCAN, PLANE_ANGLE=$PLANE_ANGLE].  This may cause flipping of the NIFTI image in the 2012 version of dcm2nii."
	exit 1
fi 


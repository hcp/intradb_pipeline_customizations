#!/bin/bash

export USER=$1
export PASSWORD=$2
export HOST=$3
export PROJECT=$4
export SUBJECT=$5
export SESSION=$6
export SCAN=$7

if [ $# -lt 7 ] ; then
   echo "1.0"
   exit 0
fi

die(){
  echo >&2 "$@"
  exit 1
}



## See if DICOM resource exists
echo curl -k -f -u $USER:$PASSWORD -X GET $HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM?format=xml
curl -k -f -u $USER:$PASSWORD -X GET $HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM?format=xml

if [ $? -eq 0 ] ; then

   	curl -k -f -u $USER:$PASSWORD -X GET $HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM/files?format=zip\&structure=legacy > ${SESSION}_${SCAN}.zip || die "Couldn't get scan DICOM files"
   	unzip ${SESSION}_${SCAN}.zip || die "Couldn't unzip downloaded file"
	## CLEAN UP ##
   	rm ${SESSION}_${SCAN}.zip 

fi

## IF THIS IS A STRUCTURAL SESSION,  GET DEFACED FILES IF THERE ARE ANY 
export SCANTYPE=`curl -k -u $USER:$PASSWORD -X GET $HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN?format=xml | grep -a " ID=" | grep -a " type=\"" | sed "s/^.* type=\"//" | sed "s/\".*$//"`
SCANTYPE=`echo $SCANTYPE | sed "s/ //g"`

## IF WE HAVE DICOM_DEFACED DIRECTORY, PULL IT

export DEFACE_RESP=`curl -k -s -L -o /dev/null -w '%{http_code}' -u $USER:$PASSWORD -X GET "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM_DEFACED?format=xml"`

if [[ $DEFACE_RESP -eq 200 ]]; then

        echo "NOTE:  Scan contains defaced DICOM output [SCAN=$SCAN].  Begin pulling it...."

else

        echo "NOTE:  No defaced DICOM output for this scan [SCAN=$SCAN]."
        exit 0

fi

pushd ../RAW_DEFACE
echo `pwd`

curl -k -f -u $USER:$PASSWORD -X GET $HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM_DEFACED/files?format=zip\&structure=legacy > ${SESSION}_${SCAN}_df.zip || die "Couldn't get scan DEFACED DICOM files"
unzip ${SESSION}_${SCAN}_df.zip || die "Couldn't unzip downloaded file"
## CLEAN UP ##
rm ${SESSION}_${SCAN}_df.zip 

popd ../RAW_DEFACE


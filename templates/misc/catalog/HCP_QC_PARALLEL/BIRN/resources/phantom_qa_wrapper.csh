#!/bin/csh

#This is a wrapper script to generate the QCAssessment element for the Phantom QA
#using the BIRN Phantom QA techinique
#Refer: https://xwiki.nbirn.net/xwiki/bin/view/Function-BIRN/AutomatedQA

if ( ${#argv} < 9 ) then
  echo "Usage: phantom_qa_wrapper <XNAT ID>  <Project Id> <Session Label> <Subject ID> <ROI Size> <Host> <User> <Password> <Path to Params file>"
  exit -1;
endif

set xnatId=$argv[1]
set project=$argv[2]
set sessionLabel=$argv[3]
set subject=$argv[4]
set roisize=$argv[5]
set host=$argv[6]
set user = $argv[7]
set pass = $argv[8]
set useremail=$argv[9]
set adminemail=$argv[10]


set siteId = "1"
set siteName = "HCPIntraDB"


set XNATRestClient = @PIPELINE_DIR_PATH@/xnat-tools/XNATRestClient
set MY_OUTLIER_RESOURCE_NAME = "level2qc_xnat_qcAssessmentData"
set BINDIR = @PIPELINE_DIR_PATH@/bin
set PIPELINE_HOME = @PIPELINE_DIR_PATH@


set wrkdir = $cwd

#############################################################################################
#
#  GET a jsession
#
##############################################################################################
set MY_CURL_OPTIONS = " -k "
set VALIDATION_XSL_FILE=$PIPELINE_HOME/catalog/validation_tools/resources/svrl/nrg_iso_svrl_for_xslt2.xsl
set OUTLIER_XSL_FILE=$PIPELINE_HOME/catalog/HCP_QC_PARALLEL/Wrapper_QC/resources/check_outliers.xsl


set jsession = `curl  $MY_CURL_OPTIONS -u ${user}:${pass} ${host}data/JSESSION`
if ($status != 0) then
	echo "Unable to get a jsession Aborting!"
	exit 1
endif


########################################
#
# GET SCAN META DETAILS
#
#########################################

set scanDetailsCSV = ${wrkdir}/scan_details.csv

set restPath = "${host}data/archive/projects/${project}/subjects/${subject}/experiments/${sessionLabel}/scans?columns=ID,type,series_description&format=csv"

curl -k -u ${user}:${pass} -G "$restPath" > $scanDetailsCSV

set schematron_folder = $wrkdir/outlier_files
mkdir -p $schematron_folder

###########################################################################################
#
#
# Validate each QC for outliers. The cut-offs would depend on project and scan type
#
# August 14, 2017: Temporarily putting off the outlier pull. Need enough data before Outliers can be set
############################################################################################

#curl $MY_CURL_OPTIONS --cookie "JSESSIONID=$jsession" -X GET  "${host}data/archive/projects/${project}/resources/${MY_OUTLIER_RESOURCE_NAME}/files?format=zip" > $wrkdir/${MY_OUTLIER_RESOURCE_NAME}.zip  


#pushd $schematron_folder
#  unzip -j $wrkdir/${MY_OUTLIER_RESOURCE_NAME}.zip 
#  cp $PIPELINE_HOME/catalog/validation_tools/resources/site_imports.xsl .
#popd

set createReport = 0	

#set session_outlier_report_file = $wrkdir/${sessionLabel}_report.dat
#touch $session_outlier_report_file

foreach s (`ls -d scan*/ | xargs -l basename`)
	set imagefile = $wrkdir/$s"_image.xml"
	set scanId = `echo $s | sed "s/scan//"`
	set date = `date "+%Y%m%d%H%M"`
	set qcAssessmentFile = $wrkdir/BIRN_AGAR_QC_$scanId.xml

	set qc_id = $xnatId"_SCAN${scanId}_BIRN_AGAR_QC"_$date
	
	mkdir -p $wrkdir/BIRN_PHANTOM_QA/$scanId
	mkdir -p $wrkdir/BIRN_HUMAN_QA/$scanId
	
	@PIPELINE_DIR_PATH@/catalog/HCP_QC_PARALLEL/BIRN/resources/phantom_qa.csh $imagefile $qcAssessmentFile $scanId $roisize $xnatId $qc_id $host $sessionLabel $project
	if ($status != 0) then
	  exit 1
	endif  

	################################
	# Find the outliers
	#
	################################

	set series_description = `grep ",/data/experiments/${xnatId}/scans/${scanId}" $scanDetailsCSV |  awk '{split($0,a,","); print a[4];}' | sed 's/"//g'`
	#Get the validation document - level2qc_xnat_qcAssessmentData/series_description_rules.sch
	set schematron_file = $schematron_folder/${series_description}_rules.sch
	if ( ! -e $schematron_file ) then
	       set schematron_file = $schematron_folder/rules.sch
	endif
	if ( -e $schematron_file ) then
		   set createReport = 1	
		   set schematron_rule_xsl_file = $schematron_folder/${series_description}_rules.xsl    
		   $PIPELINE_HOME/validation-tools/validation-transform -o $schematron_rule_xsl_file $schematron_file $VALIDATION_XSL_FILE 
		   #Check the QCAssessment XML against the Schematron rule file
		   set scanReport = $wrkdir/${scanId}_report.xml
		   $PIPELINE_HOME/validation-tools/validation-transform -o $scanReport $qcAssessmentFile $schematron_rule_xsl_file 
		   #Create a XSL which print the check results into proper format
		   $PIPELINE_HOME/validation-tools/validation-transform -o $wrkdir/${scanId}_report.txt $scanReport $OUTLIER_XSL_FILE session_label=$sessionLabel series_description=$series_description
		   echo "***********************************************************************************" >> $session_outlier_report_file 	
		   cat  $wrkdir/${scanId}_report.txt >> $session_outlier_report_file
		   echo "***********************************************************************************" >> $session_outlier_report_file 	
		   cp $wrkdir/${scanId}_report.txt $wrkdir/BIRN_PHANTOM_QA/$scanId/${scanId}_report.txt
		   cp $wrkdir/${scanId}_report.txt $wrkdir/BIRN_HUMAN_QA/$scanId/${scanId}_report.txt
		   
	 endif	

	
	################################
	# Upload the XML
	#
	################################
	set restPath = data/archive/projects/${project}/subjects/${subject}/experiments/${sessionLabel}/assessors/${qc_id}
	echo $restPath

	#$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath" -local $qcAssessmentFile

	curl -u ${user}:${pass} -H "Content-Type:text/xml" -X PUT --data-binary @$qcAssessmentFile $host/$restPath


	################################
	# Upload the various files
	#
	################################


	pushd $wrkdir/BIRN_PHANTOM_QA/$scanId

	zip -r phantom_$scanId *

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/BIRN_AGAR_DATA" 

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/BIRN_AGAR_DATA/files?extract=true\&label=BIRN_AGAR_DATA\&content=BIRN_AGAR_DATA" -local phantom_${scanId}.zip

	popd

	pushd $wrkdir/BIRN_HUMAN_QA/$scanId

	zip -r human_$scanId *

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/BIRN_DATA" 

	$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/BIRN_DATA/files?extract=true\&label=BIRN_AGAR_DATA\&content=BIRN_DATA" -local human_${scanId}.zip

	popd
end


#If any failed assert exists, email the summary file to a list of people

if ( -e $session_outlier_report_file ) then
	set failed=`grep "Outlier Check: fail" $session_outlier_report_file`
	if ( X"$failed" != X ) then
	  set text="Outliers exist for session $sessionLabel in project $project. A report for each scan is attached."
	  curl $MY_CURL_OPTIONS --cookie "JSESSIONID=$jsession"  -F "to=$useremail" -F "cc=${adminemail}" -F "cc=mharms@conte.wustl.edu" -F "cc=linda@npg.wustl.edu"  -F "text=$text" -F "subject=Outlier Exists for ${sessionLabel}" -F "file=@$session_outlier_report_file" ${host}data/services/mail/send
	endif
endif



exit 0

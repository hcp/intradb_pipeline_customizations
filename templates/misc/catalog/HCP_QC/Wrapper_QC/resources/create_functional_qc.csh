#!/bin/csh 
set echo 
set xnat_id = $argv[1]
set sessionLabel = $argv[2]
set project = $argv[3]
set subject = $argv[4]
set wrkdir = $argv[5] #Path to the STRUCTURAL folder
set scanid = $argv[6]
set host = $argv[7]
set user = $argv[8]
set pass = $argv[9]

set programName="$0"
set version=1.0

set XNATRestClient = @PIPELINE_DIR_PATH@/xnat-tools/XNATRestClient
set HUMAN_QA = @PIPELINE_DIR_PATH@/catalog/HCP_QC/BIRN/resources/human_qa.csh

set qcFileName = $wrkdir/level2QC_$scanid.xml

set date = `date "+%Y%m%d%H%M"`

set qc_id = $xnat_id"_SCAN${scanid}_LEVEL2_FUNCTIONAL_QC"_$date

set timeStamp=`date +"%Y-%m-%dT%T"`
set dateTime=`date +"%Y-%m-%d"`

set user="$USER"
set machine=`hostname`
set platform=`grep -m1 "model name" /proc/cpuinfo | awk 'BEGIN{FS=":"}{print $2}'`
set platformVersion=`grep "Linux" /proc/version`

#pushd $wrkdir/SimpleSNR/$scanid
#    set tSNR = `tail -1 *_simpleSNR_4.txt | awk '{print $2}'`
#popd



cat << EOF > $qcFileName
<!--Sample XML file generated by XMLSpy v2009 sp1 #(http://www.altova.com)-->
EOF
echo '<xnat:QCAssessment ID="'$qc_id'" type="FUNCTIONAL_QC" project="'$project'" label="'$qc_id'" xsi:schemaLocation="http://nrg.wustl.edu/xnat xnat.xsd"' >> $qcFileName
echo 'xmlns:xnat="http://nrg.wustl.edu/xnat"' >> $qcFileName
echo 'xmlns:prov="http://www.nbrin.net/prov" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' >> $qcFileName
echo '<xnat:date>'$dateTime'</xnat:date>' >> $qcFileName
#echo "<xnat:provenance>" >> $qcFileName
#echo "<prov:processStep>" >> $qcFileName
#echo "<prov:program version="\"$version\"">$programName</prov:program>" >> $qcFileName
#echo "<prov:timestamp>$timeStamp</prov:timestamp>" >> $qcFileName
#echo "<prov:user>$user</prov:user>" >> $qcFileName
#echo "<prov:machine>$machine</prov:machine>" >> $qcFileName
#echo "<prov:platform>$platform</prov:platform>" >> $qcFileName
#echo "</prov:processStep>" >> $qcFileName
#echo "</xnat:provenance>" >> $qcFileName

echo '<xnat:imageSession_ID>'$xnat_id'</xnat:imageSession_ID>' >> $qcFileName


echo '<xnat:scans>' >> $qcFileName

echo '<xnat:scan id="'$scanid'">' >> $qcFileName
echo '<xnat:scanStatistics xsi:type="xnat:statisticsData">' >> $qcFileName

###################################
# SimpleSNR outs
#
###################################


#echo '<xnat:additionalStatistics name="SIMPLESNR_tSNR">'$tSNR'</xnat:additionalStatistics>' >> $qcFileName

###################################
# Fourier Coefficients -  None
# only plots
###################################

###################################
# Motion Outliers 
# 
###################################

foreach line ( "`cat $wrkdir/MotionOutlier/$scanid/${scanid}_mo.dat`" )
  set measure = `echo $line | awk '{split($1,a,"="); print a[1]};'`
  set value = `echo $line | awk '{split($1,a,"="); print a[2]};'`
  echo '<xnat:additionalStatistics name="'$measure'">'$value'</xnat:additionalStatistics>' >> $qcFileName
end


###################################
# BIRN Human QA -  Measures
# HTML
###################################

###################################
# Wavelet Kurtosis
# only plots
###################################


set human_qa_dir = $wrkdir/BIRN

$HUMAN_QA $xnat_id $qc_id $human_qa_dir/${scanid}_image.xml $qcFileName $scanid $sessionLabel $human_qa_dir

pushd $human_qa_dir/$scanid
	sed -e 's+src="\(.*\)"+src="'$host'data/archive/experiments/'$xnat_id'/assessors/'$qc_id'/out/resources/BIRN_DATA/files/\1"+' index.html > web_access_index_temp.html
	sed -e 's+href="index.css"+href="'$host'/style/birn/index.css"+' web_access_index_temp.html > web_access_index.html
	\rm -f web_access_index_temp.html
	
popd

echo '</xnat:scanStatistics>' >> $qcFileName

echo '</xnat:scan>' >> $qcFileName

echo '</xnat:scans>' >> $qcFileName
echo '</xnat:QCAssessment>' >> $qcFileName

################################
# Upload the XML
#
################################

set restPath = data/archive/projects/${project}/subjects/${subject}/experiments/${sessionLabel}/assessors/${qc_id}
echo $restPath

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath" -local $qcFileName



################################
# Upload the various files
#
################################

################################
# Simple SNR
#
################################

#pushd $wrkdir/SimpleSNR/$scanid


#zip -r simplesnr_$scanid *

#$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/SIMPLESNR_DATA" 

#$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/SIMPLESNR_DATA/files?extract=true\&label=SIMPLESNR_DATA\&content=SIMPLESNR_DATA" -local simplesnr_$scanid.zip

#popd

################################
# Fourier Slope Statistics
#
################################

pushd $wrkdir/FourierSlope/$scanid

zip -r fourier_$scanid *

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/FOURIER_COEFFICIENTS_DATA" 

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/FOURIER_COEFFICIENTS_DATA/files?extract=true\&label=FOURIER_COEFFICIENTS_DATA\&content=FOURIER_COEFFICIENTS_DATA" -local fourier_$scanid.zip

popd

################################
# MotionOutliers
#
################################

pushd $wrkdir/MotionOutlier/$scanid

zip -r motionoutlier_$scanid *

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/MOTIONOUTLIER_DATA" 

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/MOTIONOUTLIER_DATA/files?extract=true\&label=MOTIONOUTLIER_DATA\&content=MOTIONOUTLIER_DATA" -local motionoutlier_$scanid.zip

popd

################################
# BIRN HUMAN QA
#
################################

pushd $wrkdir/BIRN/$scanid

zip -r birn_$scanid *

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/BIRN_DATA" 

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/BIRN_DATA/files?extract=true\&label=BIRN_DATA\&content=BIRN_DATA" -local birn_$scanid.zip

popd


################################
# Wavelet Statistics
#
################################

pushd $wrkdir/WaveletKurtosis/$scanid

zip -r wavelet_$scanid *

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/out/resources/WAVELETSTATISTICS_DATA" 

$XNATRestClient -host $host -u $user -p $pass -m PUT -remote "$restPath/resources/WAVELETSTATISTICS_DATA/files?extract=true\&label=WAVELETSTATISTICS_DATA\&content=WAVELETSTATISTICS_DATA" -local wavelet_$scanid.zip

popd


exit 0

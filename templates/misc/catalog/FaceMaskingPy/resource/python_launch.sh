#!/bin/bash

source /nrgpackages/scripts/epd-python_setup.sh

if [ $# -lt  7 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export PROJECT=$4
export SESSION=$5
export UNUSABLE=$6
export RUNOTHER=$7

pushd /nrgpackages/tools.release/intradb_pipe_launch

# Set up optional params
params=""
if [ $UNUSABLE = "Y" ]; then
   params+=" -U"
fi
if [ $RUNOTHER = "Y" ]; then
   params+=" -r"
fi

echo "python launch.py -u $USER -p $PASSWORD -H $HOST -P $PROJECT -s $SESSION -i facemask $params"
python launch.py -u $USER -p $PASSWORD -H $HOST -P $PROJECT -s $SESSION -i facemask $params

